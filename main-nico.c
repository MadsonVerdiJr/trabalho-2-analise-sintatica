#include <stdio.h>
#include <stdlib.h>

#include "node.h"
int yyparse();


/* Programa principal do nico. */
const char* progname;
extern int yylineno;
extern FILE* yyin;

/**
 * Função principal do sistema
 * @param  argc Quantidade de parâmetros
 * @param  argv Parâmetros
 * @return      syntax_tree
 */
int main(int argc, const char* argv[]) {
  if (argc != 2) {
    printf("Uso: %s <input_file>. Try again!\n", argv[0]);
    exit(1);
  }

  yyin = fopen(argv[1], "r");

  if (!yyin) {
    printf("Uso: %s <input_file>. Could not find '%s'. Try again!\n", argv[0], argv[1]);
    exit(2);
  }

  progname = argv[0];

  if (!yyparse()){
    printf("OKAY.\n");
  } else {
    printf("ERROR.\n");
  }

  if (syntax_tree->type == int_node) {
    printf("A AST se limita a uma folha tipo INT rotulada por: %s\n", syntax_tree->lexeme);
  } else {
    printf("Something got wrong in the AST.\n");
  }

  exit(0);
}

void yyerror(const char* s) {
  fprintf(stderr, "%s: %s (line %d)\n", progname, s, yylineno);
}
