# Trabalho 2 - Análise Sintática

## Desenvolvido por

* [Madson Verdi Jr.](mailto:madsonverdijr@gmail.com)
* [Odilon R. Machado Jr.](mailto:odilon.machadojr@gmail.com)

## Descrição

*Pede-se implementar uma estrutura de árvore generalizada (ver o arquivo node.h), e usá-la junto com um analisador sintático (parser ) a ser obtido pelo Yacc.*

**Uma árvore generalizada** é uma árvore em que cada nó pode possuir qualquer número de filhos. O arquivo node.h provê uma implementação do tipo abstrato Node (atalho de escrita para struct _node *) com as informações que se quer armazenar dentro do nó. Espera-se, entre outros procedimentos, implementações do construtor create_node, do acessor ao filho i de um nó, de um destrutor deep_free_node, de um método que retorne a altura da árvore cuja raiz seja um dado nó, e de um método que retorne o número total
de nós na árvore.

Salienta-se que se espera implementações corretas: sem perda de memória (lembrar que C não usa coletor de lixo; caso se use um malloc, deve-se usar um free em algum outro lugar), sem acesso fora da área de memória alocada, sem efeito colateral não controlado, etc. . .


**O analisador sintático**. Deve-se implementar um parser (analisador sintático) bottom-up, utilizando o Yacc, que reconhece a gramática usada no compilador Nico e cria uma árvore que representa as derivações sucessivas (Abstract Syntax Tree).

Você deve:

* Continuar a usar sua implementação de Nico da Etapa 1.
* Ler a apostila que apresenta como se usa o Yacc (no Moodle).
* Baixar os arquivos de código-base disponibilizados no Moodle.
* Copiá-los e complementá-los em Nico/src para implementar a estrutura de dados de árvore e o parser com a geração da AST.

O esforço de implementação é mínimo, mas há trabalho para entender como se faz.