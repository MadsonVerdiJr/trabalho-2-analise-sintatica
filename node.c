#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <assert.h>
#include "node.h"

Node * syntax_tree;

Node* create_node(int nl, Node_type t, char* lexeme, ...)
{
    Node* new_node = (Node *)malloc(sizeof(Node));

    assert(new_node != NULL);

    new_node->line_num = nl;
    new_node->lexeme = lexeme;
    new_node->type = t;
    new_node->child = NULL;
    new_node->n_child = 0;

    va_list ap;
    va_start(ap, lexeme);

    const Node* child;

    for (child = (Node*)va_arg(ap, const Node*); child != NULL; child = (Node*)va_arg(ap, const Node*))
    {
        new_node->n_child++;
        new_node->child = (Node **)realloc(new_node->child, sizeof(Node*) * new_node->n_child);
        new_node->child[new_node->n_child-1] = (Node*)child;
    }

    va_end(ap);

    return new_node;
}

int nb_of_children(Node* n)
{
    assert(n != NULL);
    return n->n_child;
}

int is_leaf(Node* n)
{
    assert(n != NULL);
    return n->n_child == 0;
}

/** Destructor of a Node. Desallocates (recursively) all the tree rooted at
 * 'n'.
 */
int deep_free_node(Node* n)
{
    if (n != NULL)
    {
        int i;
        for (i = 0; i < n->n_child; ++i)
        {
            deep_free_node(n->child[i]);
            free(n->child[i]);
        }
        free(n->child);
        free(n);
    }
    return 0;
}

/** returns the height of the tree rooted by 'n'.
 *  The height of a leaf is 1.
 */
int height(Node *n)
{
    assert(n != NULL);

    int h = 0;
    if (n->n_child != 0)
    {
        int i;
        for ( i = 0; i < n->n_child; ++i)
        {
            int child_h = height(n->child[i]);
            if (child_h > h)
            {
                h = child_h;
            }
        }
    }
    return h+1;
}

void uncompile(FILE* outfile, Node *n)
{
    if (n != NULL)
    {
        int i;
        for (i = 0; i < n->n_child; ++i)
        {
            uncompile(outfile, n->child[i]);
        }

        if (n->lexeme != NULL)
        {
            fprintf(outfile, "%s ", n->lexeme);
        }
    }
}
