CC      = gcc
LDFLAGS = -g

all: etapa2

## Etapa 2
etapa2: nico


doc: Makefile Doxyfile $(HEAD1)
	doxygen Doxyfile

nico: Makefile  lex.yy.c  nico.tab.o tokens.h  node.o main-nico.c
	# $(CC) $(LDFLAGS) -DYYDEBUG -Wall -o nico lex.yy.c nico.tab.o main-nico.c node.o -lfl
	$(CC) $(LDFLAGS) -DYYDEBUG -o nico lex.yy.c nico.tab.o main-nico.c node.o -lfl

lex.yy.c: Makefile scanner.l tokens.h
	flex scanner.l

tokens.h: Makefile nico.tab.c
nico.tab.c: Makefile nico.y
	bison --debug --locations --defines=tokens.h nico.y

node.o: Makefile node.c node.h
	$(CC) $(LDFLAGS) -Wall -c node.c
