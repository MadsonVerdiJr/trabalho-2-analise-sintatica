/** @file node.h
 *  @version 1.2
 */

#ifndef _NODE_H_
#define _NODE_H_

#ifdef __GNUC__
    /* If using gcc, warn about missing sentinel NULLs */
    #define NULL_TERMINATED __attribute__((sentinel))
#else
    #define NULL_TERMINATED
#endif

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>


typedef int Node_type;

/* Serie de constantes que servirao para definir tipos de nos (na arvore).
 * Essa serie pode ser completada ou alterada a vontade.
 */

///////////////////
// não terminais //
///////////////////
#define code_node               298
#define declaracoes_node        299
#define declaracao_node         300
#define listadeclaracao_node    301
#define tipo_node               302
#define tipounico_node          303
#define tipolista_node          304
#define listadupla_node         305
#define acoes_node              306
#define comando_node            307
#define lvalue_node             308
#define expr_node               309
#define bloco_node              310
#define listaexpr_node          311
#define chamaproc_node          312
#define fiminstcontrole_node    313
#define expbool_node            314

///////////////
// Terminais //
///////////////
#define semicolumn_node         400 // ';'
#define column_node             401 // ':'
#define lparenthesis_node       402 // '('
#define rparenthesis_node       403 // ')'
#define comma_node              404 // ','
#define lbrackets_node          405 // '['
#define rbrackets_node          406 // ']'
#define lbrace_node             407 // '{'
#define rbrace_node             408 // '}'

///////////
// tipos //
///////////
#define int_node                501 // 'INT'
#define intlit_node             502 // 'INT_LIT'
#define double_node             503 // 'DOUBLE'
#define real_node               504 // 'REAL'
#define flit_node               504 // 'FLIT'
#define char_node               505 // 'CHAR'
#define idf_node                506 // 'IDF'

////////////////////////////
// Operadores e condições //
////////////////////////////
#define swap_node               507 // '<=>'
#define if_node                 508 // 'if'
#define end_node                509 // 'end'
#define else_node               510 // 'else'
#define then_node               511 // 'then'
#define true_node               512 // 'true'
#define false_node              513 // 'false'
#define and_node                514 // 'and'
#define or_node                 515 // 'or'
#define not_node                516 // 'not'
#define le_node                 517 // '<='
#define ge_node                 518 // '>='
#define eq_node                 519 // '='
#define ne_node                 520 // '<>'
#define add_node                521 // '+'
#define sub_node                522 // '-'
#define mul_node                523 // '*'
#define div_node                523 // '/'
#define while_node              524 // 'while'
#define greater_node            525 // '>'
#define lower_node              526 // '<'

/** Estrutura de dados parcial para o no da arvore. */
typedef struct _node {
    int line_num;       /**< numero de linha. */
    Node_type type;
    char* lexeme;       /**< o lexema retornado pelo analizador lexical. */
    void* attribute;    /**< Qualquer coisa por enquanto. */
    struct _node** child;
    unsigned int n_child;
} Node;

extern Node * syntax_tree;

/**
 * Node constructor.
 *
 * @param nl: line number where this token was found in the source code.
 * @param t: node type (one of the values #define'd above). Must abort
 *             the program if the type is not correct.
 * @param lexeme: whatever string you want associated to this node.
 * @param children...: NULL-terminated list of pointers to children Node*'s.
 *     See the extra file 'exemplo_func_var_arg.c' for an example.
 *     Callers are expected to pass *only Node pointers* as arguments.
 *     To create a leaf, use just NULL.
 * @return a pointer to a new Node.
 */
NULL_TERMINATED
Node* create_node(int nl, Node_type t, char* lexeme, ...);

/** Accessor to the number of children of a Node.
 *  Must abort the program if 'n' is NULL.
 */
int nb_of_children(Node* n);

/** Tests if a Node is a leaf.
 *  Must abort the program if 'n' is NULL.
 *  @return 1 if n is a leaf, 0 else.
 */
int is_leaf(Node* n);

/** accessor to the i'th child of a Node.
 * @param n : the node to be consulted. Must abort the program if 'n' is NULL.
 * @param i : the number of the child that one wants. Given a node degree d,
 *       valid values for i are: 0 <= i < d.
 *       Must abort the program if i is not correct.
 * @return a pointer to a Node.
 */
Node* child(Node* n, int i) ;

/** Destructor of a Node. Deallocates
 * (recursively) all of the tree rooted at 'n'.
 */
int deep_free_node(Node* n) ;

/** Returns the height of the tree rooted by 'n'.
 *  The height of a leaf is 1.
 */
int height(Node *n) ;

/** Prints into a file the lexemes contained in the node rooted by 'n'.
 *  Lexemes must be printed in a depth-first order.
 *  @param outfile : the file to which the lexemes are printed.
 *  @param n : the root node of the tree. Must abort the program if 'n' is NULL.
 *
 */
void uncompile(FILE* outfile, Node *n) ;

#endif
