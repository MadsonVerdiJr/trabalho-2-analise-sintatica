 /* Secao das declaracoes */

%option yylineno
%{
  #define YY_USER_ACTION yylloc.first_line = yylineno;
  #define YY_USER_INIT yylineno = 1;
%}

%option noyywrap
  /* Sera copiado no .c final */

  /* O lexema F_LIT, convertido em double, deve ser copiado numa variável C global chamada VAL_DOUBLE, de tipo double. */
  double VAL_DOUBLE;

  /* O lexema INT_LIT, convertido em int, deve ser copiado numa variável C global chamada VAL_INT, de tipo int */
  int VAL_INT;
%{
    #include <stdlib.h>
    #include <string.h>

    /* este include eh importante... */
    #include "tokens.h"

%}

IDF     [a-z_][A-Za-z0-9_]*
DIGIT   [0-9]
INT_LIT [0-9]+
F_LIT   [0-9]*"."[0-9]+([eE]([+\-])?[0-9]+)?
CONST   [A-Z][A-Z0-9_]*
SPACE   [ \t\n\r\f\s]
SPACES  SPACE+
STR_LIT \"[A-Za-z0-9_\- \t\n\r\f\s]*\"

%%

"int"           { return (INT); }
"double"        { return (DOUBLE); }
"real"          { return (REAL); }
"char"          { return (CHAR); }
"string"        { return (STRING); }
"*"             { return ('*'); }
"+"             { return ('+'); }
"-"             { return ('-'); }
"/"             { return ('/'); }
","             { return (',');}
";"             { return (';');}
":"             { return (':');}
"'"             { return (QUOTE);}
"("             { return ('(');}
")"             { return (')');}
"["             { return ('[');}
"]"             { return (']');}
"{"             { return ('{');}
"}"             { return ('}');}
"<=>"           { return (SWAP); }
"<"             { return ('<'); }
">"             { return ('>'); }
"="             { return ('='); }
"<="            { return (LE); }
">="            { return (GE); }
"=="            { return (EQ); }
"<>"            { return (NE); }
"and"           { return (AND); }
"or"            { return (OR); }
"not"           { return (NOT); }
"if"            { return (IF); }
"then"          { return (THEN); }
"else"          { return (ELSE); }
"while"         { return (WHILE); }
"end"           { return (END); }
"true"          { return (TRUE); }
"false"         { return (FALSE); }

{SPACE}         {  }

{IDF}           { printf("%s\n", yytext);     return (IDF); }
{CONST}         { printf("%s\n", yytext);     return (CONST); }
{STR_LIT}       { printf("%s\n", yytext);     return (STR_LIT); }
{INT_LIT}       { VAL_INT = atoi(yytext);     return (INT_LIT); }
{F_LIT}         { VAL_DOUBLE = atof(yytext);  return (F_LIT); }



  /* Tratamento dos erros lexicais: a regra seguinte pega tudo o que nao
   * fechou com uma Regexp anterior.
   */
. { printf("Erro léxico: caractere desconhecido '%c' (%d).\n",
        yytext[0], yytext[0]); exit(70); }
%%
  /* Secao dos  Procedimentos auxiliares  */





extern FILE *yyin;
int old_main(int argc, char* argv[]) {
    int token;
    if (argc != 2) {
        printf("uso: %s <input_file>. Try again!\n", argv[0]);
        exit(50);
    }
    yyin = fopen(argv[1], "r");
    if (!yyin) {
        printf("Uso: %s <input_file>. Could not find %s. Try again!\n",
            argv[0], argv[1]);
        exit(55);
    }

    /* agora a entrada padrao eh o arquivo especificado como 1o argumento ao
     * executavel (argv[1]).
     * Soh chama o analisador lexical default fornecido pelo output do Flex:
     */
    while (token=yylex()) {
        /* neste laco, obtem-se "palavra por palavra" os tokens reconhecidos
         * pelo scanner fornecido pelo Lex. Poderia ser feita a analise
         * sintatica... Sera feito depois!
        */
        printf("Meu analisador lexical reconheceu o token %d\n", token);
    }
    return(0);
}
