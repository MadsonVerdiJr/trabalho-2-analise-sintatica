%error-verbose

%{
  /* Aqui, pode-se inserir qualquer codigo C necessario ah compilacao
   * final do parser. Sera copiado tal como esta no inicio do y.tab.c
   * gerado por Yacc.
   */
  #include <stdio.h>
  #include <stdlib.h>
  #include "node.h"

  int yylex();
  void yyerror(const char*);

%}

%union {
  char* cadeia;
  struct _node * no;
}

%type<no> code
%type<no> declaracoes
%type<no> declaracao
%type<no> listadeclaracao
%type<no> tipo
%type<no> tipounico
%type<no> tipolista
%type<no> listadupla
%type<no> acoes
%type<no> comando
%type<no> lvalue
%type<no> listaexpr
%type<no> expr
%type<no> chamaproc
%type<no> bloco
%type<no> fiminstcontrole
%type<no> expbool

%token<cadeia> IDF
%token<cadeia> INT
%token<cadeia> DOUBLE
%token<cadeia> REAL
%token<cadeia> CHAR
%token<cadeia> QUOTE
%token<cadeia> SWAP
%token<cadeia> LE
%token<cadeia> GE
%token<cadeia> EQ
%token<cadeia> NE

%token<cadeia> AND
%token<cadeia> OR
%token<cadeia> NOT
%token<cadeia> IF
%token<cadeia> THEN
%token<cadeia> ELSE
%token<cadeia> WHILE
%token<cadeia> INT_LIT
%token<cadeia> F_LIT
%token<cadeia> END
%token<cadeia> TRUE
%token<cadeia> FALSE
%token<cadeia> STRING
%token<cadeia> CONST
%token<cadeia> STR_LIT

%token<cadeia> '+'
%token<cadeia> '-'
%token<cadeia> '*'
%token<cadeia> '/'
%token<cadeia> '='

%token<cadeia> '>'
%token<cadeia> '<'

%token<cadeia> ','
%token<cadeia> ';'
%token<cadeia> ':'

%token<cadeia> '('
%token<cadeia> ')'
%token<cadeia> '{'
%token<cadeia> '}'
%token<cadeia> '['
%token<cadeia> ']'

%start code


%%
code: declaracoes acoes {
                          $$ = create_node(@$.first_line, code_node, NULL, $1, $2, NULL);
                          syntax_tree = $$;
                        }
    | acoes {
              $$ = $1;
              syntax_tree = $$;
            }
    ;

declaracoes: declaracao ';' {
                              Node* _semicolumn = create_node(@2.first_line, semicolumn_node, $2, NULL);
                              $$ = create_node(@$.first_line, declaracoes_node, NULL, $1, _semicolumn, NULL);
                            }
           | declaracoes declaracao ';' {
                                          Node* _semicolumn = create_node(@1.first_line, semicolumn_node, $3, NULL);
                                          $$ = create_node(@$.first_line, declaracoes_node, NULL, $1, $2, _semicolumn, NULL);
                                        }
           ;

declaracao: tipo ':' listadeclaracao {
                                        Node* _column = create_node(@2.first_line, column_node, $2, NULL);
                                        $$ = create_node(@$.first_line, declaracao_node, NULL, $1, _column, $3, NULL);
                                      }

listadeclaracao: IDF {
                        Node* _idf = create_node(@1.first_line, idf_node, $1, NULL);
                        $$ = create_node(@$.first_line, listadeclaracao_node, NULL, _idf, NULL);
                      }
               | IDF ',' listadeclaracao {
                                            Node* _idf = create_node(@1.first_line, idf_node, $1, NULL);
                                            Node* _comma = create_node(@2.first_line, comma_node, $2, NULL);
                                            $$ = create_node(@1. first_line, listadeclaracao_node, NULL, _idf, _comma, $3, NULL);
                                          }
               ;

tipo: tipounico { $$ = create_node(@$.first_line, tipo_node, NULL, $1, NULL); }
    | tipolista { $$ = create_node(@$.first_line, tipo_node, NULL, $1, NULL); }
    ;

tipounico: INT  {
                  Node* _int = create_node(@1.first_line, int_node, $1, NULL);
                  $$ = create_node(@$.first_line, tipounico_node, NULL, _int, NULL);
                }
         | DOUBLE {
                    Node* _double = create_node(@1.first_line, double_node, $1, NULL);
                    $$ = create_node(@$.first_line, tipounico_node, NULL, _double, NULL);
                  }
         | REAL {
                  Node* _real = create_node(@1.first_line, real_node, $1, NULL);
                  $$ = create_node(@$.first_line, tipounico_node, NULL, _real, NULL);
                }
         | CHAR {
                  Node* _char = create_node(@1.first_line, char_node, $1, NULL);
                  $$ = create_node(@$.first_line, tipounico_node, NULL, _char, NULL);
                }
         ;

tipolista: INT '(' listadupla ')' {
                                    Node* _int = create_node(@1.first_line, int_node, $1, NULL);
                                    Node* _lparenthesis = create_node(@2.first_line, lparenthesis_node, $2, NULL);
                                    Node* _rparenthesis = create_node(@4.first_line, rparenthesis_node, $4, NULL);
                                    $$ = create_node(@$.first_line, tipolista_node, NULL, _int, _lparenthesis, $3, _rparenthesis, NULL);
                                  }
         | DOUBLE '(' listadupla ')'  {
                                        Node* _double = create_node(@1.first_line, double_node, $1, NULL);
                                        Node* _lparenthesis = create_node(@2.first_line, lparenthesis_node, $2, NULL);
                                        Node* _rparenthesis = create_node(@4.first_line, rparenthesis_node, $4, NULL);
                                        $$ = create_node(@$.first_line, tipolista_node, NULL, _double, _lparenthesis, $3, _rparenthesis, NULL);
                                      }
         | REAL '(' listadupla ')'  {
                                      Node* _real = create_node(@1.first_line, real_node, $1, NULL);
                                      Node* _lparenthesis = create_node(@2.first_line, lparenthesis_node, $2, NULL);
                                      Node* _rparenthesis = create_node(@4.first_line, rparenthesis_node, $4, NULL);
                                      $$ = create_node(@$.first_line, tipolista_node, NULL, _real, _lparenthesis, $3, _rparenthesis, NULL);
                                    }
         | CHAR '(' listadupla ')'  {
                                      Node* _char = create_node(@1.first_line, char_node, $1, NULL);
                                      Node* _lparenthesis = create_node(@2.first_line, lparenthesis_node, $2, NULL);
                                      Node* _rparenthesis = create_node(@4.first_line, rparenthesis_node, $4, NULL);
                                      $$ = create_node(@$.first_line, tipolista_node, NULL, _char, _lparenthesis, $3, _rparenthesis, NULL);
                                    }
         ;

listadupla: INT_LIT ':' INT_LIT {
                                  Node* _intlit = create_node(@1.first_line, intlit_node, $1, NULL);
                                  Node* _column = create_node(@2.first_line, column_node, $2, NULL);
                                  Node* _intlit2 = create_node(@3.first_line, intlit_node, $3, NULL);
                                  $$ = create_node(@$.first_line, listadupla_node, NULL, _intlit, _column, _intlit, NULL);
                                }
          | INT_LIT ':' INT_LIT ',' listadupla  {
                                                  Node* _intlit = create_node(@1.first_line, intlit_node, $1, NULL);
                                                  Node* _column = create_node(@2.first_line, comma_node, $2, NULL);
                                                  Node* _intlit2 = create_node(@3.first_line, intlit_node, $3, NULL);
                                                  Node* _comma = create_node(@4.first_line, comma_node, $4, NULL);
                                                  $$ = create_node(@$.first_line, listadupla_node, NULL, _intlit, _column, _intlit2, _comma, $5, NULL);
                                                }
          ;

acoes: comando  { $$ = $1; }
     | comando acoes  {
                        $$ = create_node(@$.first_line, acoes_node, NULL, $1, $2, NULL);
                      }
     ;

comando: lvalue '=' expr ';'  {
                                Node* _eq = create_node(@2.first_line, eq_node, $1, NULL);
                                Node* _semicolumn = create_node(@4.first_line, semicolumn_node, $4, NULL);
                                $$ = create_node(@$.first_line, comando_node, NULL, $1, _eq, $3, _semicolumn, NULL);
                              }
       | lvalue SWAP lvalue ';' {
                                  Node* _swap = create_node(@2.first_line, swap_node, $2, NULL);
                                  Node* _semicolumn = create_node(@4.first_line, semicolumn_node, $4, NULL);
                                  $$ = create_node(@$.first_line, comando_node, NULL, $1, _swap, $3, _semicolumn);
                                }
       | expr ';' {
                    Node* _semicolumn = create_node(@2.first_line, semicolumn_node, $2, NULL);
                    $$ = create_node(@$.first_line, comando_node, NULL, $1, _semicolumn);
                  }
       | bloco  {
                  $$ = create_node(@$.first_line, comando_node, NULL, $1, NULL);
                }
       ;

lvalue: IDF {
              Node* _idf = create_node(@1.first_line, idf_node, $1, NULL);
              $$ = create_node(@$.first_line, lvalue_node, NULL, _idf, NULL);
            }
      | IDF '[' listaexpr ']' {
                                Node* _idf = create_node(@1.first_line, idf_node, $1, NULL);
                                Node* _lbracket = create_node(@2.first_line, lbrackets_node, $2, NULL);
                                Node* _rbracket = create_node(@2.first_line, rbrackets_node, $2, NULL);
                                $$ = create_node(@$.first_line, lvalue_node, NULL, _idf, _lbracket, $3, _rbracket, NULL);
                              }
      ;

listaexpr: expr {
                  $$ = create_node(@$.first_line, listaexpr_node, NULL, $1);
                }
         | expr ',' listaexpr {
                                Node* _comma = create_node(@2.first_line, comma_node, $2, NULL);
                                $$ = create_node(@$.first_line, listaexpr_node, NULL, $1, _comma, $3, NULL);
                              }
         ;

expr: expr '+' expr {
                      Node* _add = create_node(@2.first_line, add_node, $2, NULL);
                      $$ = create_node(@$.first_line, expr_node, NULL, $1, _add, $2, NULL);
                    }
    | expr '-' expr {
                      Node* _sub = create_node(@2.first_line, sub_node, $2, NULL);
                      $$ = create_node(@$.first_line, expr_node, NULL, $1, _sub, $2, NULL);
                    }
    | expr '*' expr {
                      Node* _mul = create_node(@2.first_line, mul_node, $2, NULL);
                      $$ = create_node(@$.first_line, expr_node, NULL, $1, _mul, $2, NULL);
                    }
    | expr '/' expr {
                      Node* _div = create_node(@2.first_line, div_node, $2, NULL);
                      $$ = create_node(@$.first_line, expr_node, NULL, $1, _div, $2, NULL);
                    }
    | '(' expr ')'  {
                      Node* _lparenthesis = create_node(@1.first_line, lparenthesis_node, $1, NULL);
                      Node* _rparenthesis = create_node(@3.first_line, rparenthesis_node, $3, NULL);
                      $$ = create_node(@$.first_line, expr_node, NULL, _lparenthesis, $2, _rparenthesis);
                    }
    | INT_LIT {
                Node* _intlit = create_node(@1.first_line, intlit_node, $1, NULL);
                $$ = create_node(@$.first_line, expr_node, NULL, _intlit, NULL);
              }
    | F_LIT {
              Node* _flit = create_node(@1.first_line, flit_node, $1, NULL);
              $$ = create_node(@$.first_line, expr_node, NULL, _flit, NULL);
            }
    | lvalue  {
                $$ = create_node(@$.first_line, expr_node, NULL, $1, NULL);
              }
    | chamaproc {
                  $$ = create_node(@$.first_line, expr_node, NULL, $1, NULL);
                }
    ;

chamaproc: IDF '(' listaexpr ')'  {
                                    Node* _idf = create_node(@1.first_line, idf_node, $1, NULL);
                                    Node* _lparenthesis = create_node(@2.first_line, lparenthesis_node, $2, NULL);
                                    Node* _rparenthesis = create_node(@4.first_line, rparenthesis_node, $4, NULL);
                                    $$ = create_node(@$.first_line, chamaproc_node, NULL, _idf, _lparenthesis, $3, _rparenthesis);
                                  }
         ;

bloco: IF '(' expbool ')' THEN acoes fiminstcontrole  {
                                                        Node* _if = create_node(@1.first_line, if_node, $1, NULL);
                                                        Node* _lparenthesis = create_node(@2.first_line, lparenthesis_node, $2, NULL);
                                                        Node* _rparenthesis = create_node(@4.first_line, rparenthesis_node, $4, NULL);
                                                        Node* _then = create_node(@5.first_line, then_node, $5, NULL);
                                                        $$ = create_node(@$.first_line, bloco_node, NULL, _if, _lparenthesis, $3, _rparenthesis, _then, $6, $7, NULL);
                                                      }
     | WHILE '(' expbool ')' '{' acoes '}'  {
                                              Node* _while = create_node(@1.first_line, while_node, $1, NULL);
                                              Node* _lparenthesis = create_node(@2.first_line, lparenthesis_node, $2, NULL);
                                              Node* _rparenthesis = create_node(@4.first_line, rparenthesis_node, $4, NULL);
                                              Node* _lbrace = create_node(@5.first_line, lbrace_node, $5, NULL);
                                              Node* _rbrace = create_node(@7.first_line, rbrace_node, $7, NULL);
                                              $$ = create_node(@$.first_line, bloco_node, NULL, _while, _lparenthesis, $3, _rparenthesis, _lbrace, $6, _rbrace, NULL);
                                            }
     ;

fiminstcontrole: END  {
                        Node* _end = create_node(@1.first_line, end_node, $1, NULL);
                        $$ = create_node(@$.first_line, fiminstcontrole_node, NULL, _end, NULL);
                      }
               | ELSE acoes END {
                                  Node* _else = create_node(@1.first_line, else_node, $1, NULL);
                                  Node* _end = create_node(@3.first_line, end_node, $3, NULL);
                                  $$ = create_node(@$.first_line, fiminstcontrole_node, _else, $2, _end, NULL);
                                }
               ;

expbool: TRUE {
                Node* _true = create_node(@1.first_line, true_node, $1, NULL);
                $$ = create_node(@$.first_line, expbool_node, NULL, _true, NULL);
              }
       | FALSE  {
                  Node* _false = create_node(@1.first_line, false_node, $1, NULL);
                  $$ = create_node(@$.first_line, expbool_node, NULL, _false, NULL);
                }
       | '(' expbool ')'  {
                            Node* _lparenthesis = create_node(@1.first_line, lparenthesis_node, $1, NULL);
                            Node* _rparenthesis = create_node(@3.first_line, rparenthesis_node, $3, NULL);
                            $$ = create_node(@$.first_line, expbool_node, NULL, _lparenthesis, $2, _rparenthesis);
                          }
       | expbool AND expbool  {
                                Node* _and = create_node(@2.first_line, and_node, $2, NULL);
                                $$ = create_node(@$.first_line, expbool_node, NULL, $1, _and, $3, NULL);
                              }
       | expbool OR expbool {
                              Node* _or = create_node(@2.first_line, or_node, $2, NULL);
                              $$ = create_node(@$.first_line, expbool_node, NULL, $1, _or, $3, NULL);
                            }
       | NOT expbool  {
                        Node* _not = create_node(@1.first_line, not_node, $1, NULL);
                        $$ = create_node(@$.first_line, expbool_node, NULL, _not, $2, NULL);
                      }
       | expr '>' expr  {
                          Node* _greater = create_node(@2.first_line, greater_node, $2, NULL);
                          $$ = create_node(@$.first_line, expbool_node, NULL, $1, _greater, $3, NULL);
                        }
       | expr '<' expr  {
                          Node* _lower = create_node(@2.first_line, lower_node, $2, NULL);
                          $$ = create_node(@$.first_line, expbool_node, NULL, $1, _lower, $3, NULL);
                        }
       | expr LE expr {
                        Node* _le = create_node(@2.first_line, le_node, $2, NULL);
                        $$ = create_node(@$.first_line, expbool_node, NULL, $1, _le, $3, NULL);
                      }
       | expr GE expr {
                        Node* _ge = create_node(@2. first_line, ge_node, $2, NULL);
                        $$ = create_node(@$.first_line, expbool_node, NULL, $1, _ge, $3, NULL);
                      }
       | expr EQ expr {
                        Node* _eq = create_node(@2.first_line, eq_node, $2, NULL);
                        $$ = create_node(@$.first_line, expbool_node, NULL, $1, _eq, $3, NULL);
                      }
       | expr NE expr {
                        Node* _ne = create_node(@2.first_line, ne_node, $2, NULL);
                        $$ = create_node(@$.first_line, expbool_node, NULL, $1, _ne, $3, NULL);
                      }
       ;
%%

 /* A partir daqui, insere-se qlqer codigo C necessario. */
